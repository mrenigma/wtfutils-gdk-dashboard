#!/bin/bash

cd [PATH_TO_GITLAB_INSTALL]

status=$(gdk status)

while IFS= read -r line; do
	if [[ $line == "run:"* ]]; then
		echo -en "\033[0;32m[✔] "
	else
		echo -en "\033[0;31m[✘] "
	fi

    echo -e "$line" | sed "s/^.*\.\/services\/\([^\:]*\).*$/\1/"
done <<< "$status"
