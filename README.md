# How to use

1. Install [WTF Utils](https://wtfutil.com/getting_started/installation/)
2. Either:
    1. Start up and then close WTF (`wtfutils`) to generate the correct directory structure and copy these files into `~/.config/wtf/`
    2. Clone this repo into `~/.config/wtf/`
3. Replace all `[VAR]` instances in the configuration files with the correct values
    1. `[FULL_PATH_TO_~]` -> The full path to your users directory
    2. `[PATH_TO_GITLAB_INSTALL]` -> The full path to where the GDK and GitLab are installed
    3. `[DOCKER_CONTAINER_ID]` -> Can be used for any docker container, needs to be a running docker container's ID. I use it for the SAML docker container
4. Run `wtfutils` to open the dashboard
